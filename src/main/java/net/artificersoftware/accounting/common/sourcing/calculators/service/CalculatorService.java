package net.artificersoftware.accounting.common.sourcing.calculators.service;

import net.artificersoftware.accounting.common.sourcing.aggregate.Aggregate;
import net.artificersoftware.accounting.common.sourcing.calculators.Calculator;
import net.artificersoftware.accounting.common.sourcing.events.Event;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalculatorService {

    private final List<Calculator> calculators;

    public CalculatorService(List<Calculator> calculators) {
        this.calculators = calculators;
    }

    public void handle(Event event, Aggregate aggregate) {

    }
}
