package net.artificersoftware.accounting.common.sourcing.events.service;

import net.artificersoftware.accounting.common.sourcing.events.Event;
import net.artificersoftware.accounting.common.sourcing.events.repository.EventRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.stream.Stream;

@Service
public class EventService {

    private EventRepository repository;

    public EventService(EventRepository repository) {
        this.repository = repository;
    }

    public Stream<Event> findAllByCreationDateBeforeOrderByCreationDateAsc(LocalDateTime localDateTime) {
        return repository.findAllByCreationDateBeforeOrderByCreationDateAsc(localDateTime);
    }
}
