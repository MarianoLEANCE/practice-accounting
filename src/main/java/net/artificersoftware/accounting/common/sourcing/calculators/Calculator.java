package net.artificersoftware.accounting.common.sourcing.calculators;

import net.artificersoftware.accounting.common.sourcing.aggregate.Aggregate;
import net.artificersoftware.accounting.common.sourcing.events.Event;
import net.artificersoftware.accounting.common.sourcing.reactors.Reactor;

import java.util.List;

/**
 * Reads an event and performs operations on an aggregate.
 * Updates the aggregate synchronization date.
 *
 * @param <E> the event type
 * @param <R> the return type
 */
public abstract class Calculator<E extends Event, R> {

    private final Class eventClass;
    private final List<Reactor<E>> reactors;

    public Calculator(Class eventClass, List<Reactor<E>> reactors) {
        this.eventClass = eventClass;
        this.reactors = reactors;
    }

    /**
     * Updates the aggregate according to some rules.
     * Updates the aggregate synchronization date.
     * Does not trigger side effects.
     *
     * @param event     the event
     * @param aggregate the aggregate to update
     */
    public R calculateAndUpdateAndReact(E event, Aggregate aggregate) {
        R result = calculate(event, aggregate);
        aggregate.setSynchronizationDate(event.getCreationDate());
        reactors.forEach(reactor -> reactor.react(event));
        return result;
    }

    public void calculateAndUpdate(E event, Aggregate aggregate) {
        calculate(event, aggregate);
        aggregate.setSynchronizationDate(event.getCreationDate());
    }

    protected abstract R calculate(E event, Aggregate aggregate);

    /**
     * Wether or not this calculator handles the event.
     *
     * @param event an event
     * @return true if this calculator handles the event
     */
    public Boolean handle(Event event) {
        return eventClass.equals(event.getClass());
    }
}
