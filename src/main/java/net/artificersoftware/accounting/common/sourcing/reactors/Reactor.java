package net.artificersoftware.accounting.common.sourcing.reactors;

import net.artificersoftware.accounting.common.sourcing.events.Event;

/**
 * A reactor triggers side effects after a calculation is done.
 * Therefore reactors are triggered only when a controller event fires.
 * On the other hand they never trigger when synchronizing an aggregate.
 *
 * @param <T>
 */
public interface Reactor<T extends Event> {

    /**
     * Triggers side effects for event.
     *
     * @param event the event
     */
    void react(T event);
}
