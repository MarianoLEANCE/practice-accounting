package net.artificersoftware.accounting.common.sourcing.aggregate.service;

import net.artificersoftware.accounting.common.sourcing.aggregate.Aggregate;
import net.artificersoftware.accounting.common.sourcing.aggregate.repository.AggregateRepository;
import net.artificersoftware.accounting.common.sourcing.calculators.Calculator;
import net.artificersoftware.accounting.common.sourcing.events.Event;
import net.artificersoftware.accounting.common.sourcing.events.repository.EventRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AggregateService {

    private final AggregateRepository aggregateRepository;
    private final EventRepository eventRepository;
    private final List<Calculator> calculators;

    public AggregateService(AggregateRepository aggregateRepository,
                            EventRepository eventRepository,
                            List<Calculator> calculators) {
        this.aggregateRepository = aggregateRepository;
        this.eventRepository = eventRepository;
        this.calculators = calculators;
    }

    public Aggregate create(String name) {
        Aggregate aggregate = new Aggregate();
        aggregate.setName(name);
        aggregateRepository.save(aggregate);
        return aggregate;
    }

    public Optional<Aggregate> find(String name) {
        return aggregateRepository.findByName(name);
    }

    public List<Aggregate> list() {
        List<Aggregate> aggregates = new ArrayList<>();
        aggregateRepository.findAll().forEach(aggregates::add);
        return aggregates;
    }

    public Aggregate synchronize(Aggregate aggregate, LocalDateTime synchronizationDate) {
        eventRepository.findAllByCreationDateBeforeOrderByCreationDateAsc(synchronizationDate)
                .forEach(event -> resolve(event)
                        .ifPresent(calculator -> calculator.calculateAndUpdate(event, aggregate)));
        return aggregate;
    }

    /**
     * Find which calculator handles the event
     *
     * @param event an event
     * @return a calculator
     */
    private Optional<Calculator> resolve(Event event) {
        return calculators.stream().filter(calculator -> calculator.handle(event)).findAny();
    }
}
