package net.artificersoftware.accounting.common.sourcing.aggregate.controller.dto;

import net.artificersoftware.accounting.common.sourcing.aggregate.Aggregate;

import java.time.LocalDateTime;

public class AggregateDTO {

    private final Integer id;
    private final String name;
    private final LocalDateTime synchronizationDate;

    public AggregateDTO(Aggregate aggregate) {
        id = aggregate.getId();
        name = aggregate.getName();
        synchronizationDate = aggregate.getSynchronizationDate();
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getSynchronizationDate() {
        return synchronizationDate;
    }
}
