package net.artificersoftware.accounting.common.sourcing.events;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * A base event class to be extended.
 * Events are persisted to the database and are immutable.
 */
@Entity
public class Event {

    @Id
    @GeneratedValue
    private Integer id;
    private LocalDateTime creationDate;

    public Event() {
        this.creationDate = LocalDateTime.now();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }
}
