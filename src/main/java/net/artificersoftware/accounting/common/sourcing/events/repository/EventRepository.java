package net.artificersoftware.accounting.common.sourcing.events.repository;

import net.artificersoftware.accounting.common.sourcing.events.Event;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.stream.Stream;

@Repository
public interface EventRepository extends CrudRepository<Event, Integer> {

    Stream<Event> findAllByCreationDateBetweenOrderByCreationDateAsc(LocalDateTime from, LocalDateTime to);

    Stream<Event> findAllByCreationDateBeforeOrderByCreationDateAsc(LocalDateTime until);
}
