package net.artificersoftware.accounting.common.sourcing.aggregate.repository;

import net.artificersoftware.accounting.common.sourcing.aggregate.Aggregate;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AggregateRepository extends CrudRepository<Aggregate, Integer> {

    Optional<Aggregate> findByName(String name);
}
