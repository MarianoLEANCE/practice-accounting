package net.artificersoftware.accounting.common.sourcing.aggregate.controller.request;

import java.time.LocalDateTime;

public class SynchronizeRequest {

    private LocalDateTime synchronizationDate;

    public LocalDateTime getSynchronizationDate() {
        return synchronizationDate;
    }

    public void setSynchronizationDate(LocalDateTime synchronizationDate) {
        this.synchronizationDate = synchronizationDate;
    }
}
