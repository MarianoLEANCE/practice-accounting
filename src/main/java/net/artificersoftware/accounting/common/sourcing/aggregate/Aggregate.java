package net.artificersoftware.accounting.common.sourcing.aggregate;

import net.artificersoftware.accounting.common.sourcing.calculators.service.CalculatorService;
import net.artificersoftware.accounting.common.sourcing.events.service.EventService;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * The state of an application at a given time.
 * There should be a single aggregate of type LIVE.
 * There can be any number of aggregate of type SNAPSHOT.
 * <p>
 * The LIVE aggregate represents the current state of the application.
 * It is updated immediately by calculators and reactors.
 * <p>
 * The snapshots are generated on demand.
 * <p>
 * If the LIVE aggregate is not present it will be created and synced up to the last event.
 */
@Entity
public class Aggregate {

    private Integer id;
    private LocalDateTime synchronizationDate;
    private String name;

    // methods

    public void syncUpTo(LocalDateTime localDateTime,
                         EventService eventService,
                         CalculatorService calculatorService) {
        eventService.findAllByCreationDateBeforeOrderByCreationDateAsc(localDateTime)
                .forEach(event -> calculatorService.handle(event, this));
    }

    // properties

    @Id
    @GeneratedValue
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getSynchronizationDate() {
        return synchronizationDate;
    }

    public void setSynchronizationDate(LocalDateTime synchronizationDate) {
        this.synchronizationDate = synchronizationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
