package net.artificersoftware.accounting.common.sourcing.aggregate.controller;

import net.artificersoftware.accounting.common.sourcing.aggregate.controller.dto.AggregateDTO;
import net.artificersoftware.accounting.common.sourcing.aggregate.controller.request.SynchronizeRequest;
import net.artificersoftware.accounting.common.sourcing.aggregate.service.AggregateService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping
public class AggregateController {

    private final AggregateService service;

    public AggregateController(AggregateService service) {
        this.service = service;
    }

    @GetMapping
    @Transactional
    public ResponseEntity<List<AggregateDTO>> list() {
        return ResponseEntity.ok(service.list().stream()
                .map(AggregateDTO::new)
                .collect(Collectors.toList()));
    }

    @GetMapping("/{name}")
    @Transactional
    public ResponseEntity<AggregateDTO> get(@PathVariable("name") String name) {
        return service.find(name)
                .map(AggregateDTO::new)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @PostMapping("/{name}")
    @Transactional
    public ResponseEntity<AggregateDTO> create(@PathVariable("name") String name) {
        return service.find(name)
                .map(aggregate -> new ResponseEntity<>(new AggregateDTO(aggregate), HttpStatus.CONFLICT))
                .orElseGet(() -> new ResponseEntity<>(new AggregateDTO(service.create(name)), HttpStatus.CREATED));
    }

    @PutMapping("/{name}/synchronize")
    @Transactional
    public ResponseEntity<AggregateDTO> synchronize(@PathVariable("name") String name,
                                                    @RequestBody SynchronizeRequest request) {
        return service.find(name)
                .map(aggregate -> service.synchronize(aggregate, request.getSynchronizationDate()))
                .map(AggregateDTO::new)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }
}
