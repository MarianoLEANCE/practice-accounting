package net.artificersoftware.accounting.account.repository;

import net.artificersoftware.accounting.account.sourcing.event.MoneyWithdrawn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MoneyWithdrawnRepository extends CrudRepository<MoneyWithdrawn, Integer> {}
