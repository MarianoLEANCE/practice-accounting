package net.artificersoftware.accounting.account.repository;

import net.artificersoftware.accounting.account.sourcing.event.MoneyDeposited;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MoneyDepositedRepository extends CrudRepository<MoneyDeposited, Integer> {}
