package net.artificersoftware.accounting.account.repository;

import net.artificersoftware.accounting.account.model.Account;
import net.artificersoftware.accounting.account.model.AccountId;
import net.artificersoftware.accounting.common.sourcing.aggregate.Aggregate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<Account, AccountId> {

    List<Account> findAllByIdAggregate(Aggregate aggregate);
}
