package net.artificersoftware.accounting.account.repository;

import net.artificersoftware.accounting.account.sourcing.event.AccountCreated;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountCreatedRepository extends CrudRepository<AccountCreated, Integer> {}
