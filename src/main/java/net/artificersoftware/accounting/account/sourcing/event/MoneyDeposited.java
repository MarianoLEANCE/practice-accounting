package net.artificersoftware.accounting.account.sourcing.event;

import net.artificersoftware.accounting.common.sourcing.events.Event;

import javax.persistence.Entity;

/**
 * Event of money deposited on an account
 */
@Entity
public class MoneyDeposited extends Event {

    private String number;
    private Integer amount;

    /**
     * @param number the account number
     * @param amount the amount deposited
     */
    public MoneyDeposited(String number, Integer amount) {
        super();
        this.number = number;
        this.amount = amount;
    }

    /**
     * For persistence only
     */
    public MoneyDeposited() {
        super();
    }

    public String getNumber() {
        return number;
    }

    /**
     * For persistence only
     */
    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getAmount() {
        return amount;
    }

    /**
     * For persistence only
     */
    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
