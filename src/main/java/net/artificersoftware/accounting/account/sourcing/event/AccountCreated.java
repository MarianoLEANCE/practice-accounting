package net.artificersoftware.accounting.account.sourcing.event;

import net.artificersoftware.accounting.common.sourcing.events.Event;

import javax.persistence.Entity;

/**
 * Event of the creation of an account
 */
@Entity
public class AccountCreated extends Event {

    private String number;
    private Integer balance;

    // constructors

    /**
     * @param number  the account number
     * @param balance the balance to start with
     */
    public AccountCreated(String number, Integer balance) {
        super();
        this.number = number;
        this.balance = balance;
    }

    /**
     * For persistence only
     */
    public AccountCreated() {
        super();
    }

    // properties

    public String getNumber() {
        return number;
    }

    /**
     * For persistence only
     */
    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getBalance() {
        return balance;
    }

    /**
     * For persistence only
     */
    public void setBalance(Integer balance) {
        this.balance = balance;
    }
}
