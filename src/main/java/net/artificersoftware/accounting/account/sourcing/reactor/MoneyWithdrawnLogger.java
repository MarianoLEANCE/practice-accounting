package net.artificersoftware.accounting.account.sourcing.reactor;

import net.artificersoftware.accounting.account.sourcing.event.MoneyWithdrawn;
import net.artificersoftware.accounting.common.sourcing.reactors.Reactor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MoneyWithdrawnLogger implements Reactor<MoneyWithdrawn> {

    private final Logger logger = LoggerFactory.getLogger(MoneyWithdrawnLogger.class);

    @Override
    public void react(MoneyWithdrawn event) {
        logger.info(String.format("Withdraw %d on account %s", event.getAmount(), event.getNumber()));
    }
}
