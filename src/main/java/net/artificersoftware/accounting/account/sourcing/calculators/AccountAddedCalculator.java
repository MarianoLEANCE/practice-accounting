package net.artificersoftware.accounting.account.sourcing.calculators;

import net.artificersoftware.accounting.account.model.Account;
import net.artificersoftware.accounting.account.repository.AccountRepository;
import net.artificersoftware.accounting.account.sourcing.event.AccountCreated;
import net.artificersoftware.accounting.common.sourcing.aggregate.Aggregate;
import net.artificersoftware.accounting.common.sourcing.calculators.Calculator;
import net.artificersoftware.accounting.common.sourcing.reactors.Reactor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AccountAddedCalculator extends Calculator<AccountCreated, Account> {

    private final AccountRepository accountRepository;

    public AccountAddedCalculator(List<Reactor<AccountCreated>> reactors,
                                  AccountRepository accountRepository) {
        super(AccountCreated.class, reactors);
        this.accountRepository = accountRepository;
    }

    public Account calculate(AccountCreated event, Aggregate aggregate) {
        Account account = new Account(aggregate, event.getNumber(), event.getBalance());
        accountRepository.save(account);
        return account;
    }
}
