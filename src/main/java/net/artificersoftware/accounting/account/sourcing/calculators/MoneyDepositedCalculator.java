package net.artificersoftware.accounting.account.sourcing.calculators;

import net.artificersoftware.accounting.account.model.Account;
import net.artificersoftware.accounting.account.model.AccountId;
import net.artificersoftware.accounting.account.repository.AccountRepository;
import net.artificersoftware.accounting.account.sourcing.event.MoneyDeposited;
import net.artificersoftware.accounting.common.sourcing.aggregate.Aggregate;
import net.artificersoftware.accounting.common.sourcing.calculators.Calculator;
import net.artificersoftware.accounting.common.sourcing.reactors.Reactor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class MoneyDepositedCalculator extends Calculator<MoneyDeposited, Optional<Account>> {

    private final AccountRepository accountRepository;

    public MoneyDepositedCalculator(List<Reactor<MoneyDeposited>> reactors,
                                    AccountRepository accountRepository) {
        super(MoneyDeposited.class, reactors);
        this.accountRepository = accountRepository;
    }

    public Optional<Account> calculate(MoneyDeposited event, Aggregate aggregate) {
        Optional<Account> account = accountRepository.findById(new AccountId(aggregate, event.getNumber()));
        account.ifPresent(a -> a.deposit(event.getAmount()));
        return account;
    }
}
