package net.artificersoftware.accounting.account.sourcing.event;

import net.artificersoftware.accounting.common.sourcing.events.Event;

import javax.persistence.Entity;

/**
 * Event of money withdrawn from an account.
 */
@Entity
public class MoneyWithdrawn extends Event {

    private String number;
    private Integer amount;

    /**
     * @param number the account number
     * @param amount the amount withdrawn
     */
    public MoneyWithdrawn(String number, Integer amount) {
        super();
        this.number = number;
        this.amount = amount;
    }

    /**
     * For persistence only
     */
    public MoneyWithdrawn() {
        super();
    }

    public String getNumber() {
        return number;
    }

    /**
     * For persistence only
     */
    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getAmount() {
        return amount;
    }

    /**
     * For persistence only
     */
    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
