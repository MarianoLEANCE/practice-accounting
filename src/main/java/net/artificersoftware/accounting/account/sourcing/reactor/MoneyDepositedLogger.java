package net.artificersoftware.accounting.account.sourcing.reactor;

import net.artificersoftware.accounting.account.sourcing.event.MoneyDeposited;
import net.artificersoftware.accounting.common.sourcing.reactors.Reactor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MoneyDepositedLogger implements Reactor<MoneyDeposited> {

    private final Logger logger = LoggerFactory.getLogger(MoneyDepositedLogger.class);

    @Override
    public void react(MoneyDeposited event) {
        logger.info(String.format("Deposit %d on account %s", event.getAmount(), event.getNumber()));
    }
}
