package net.artificersoftware.accounting.account.sourcing.reactor;

import net.artificersoftware.accounting.account.sourcing.event.AccountCreated;
import net.artificersoftware.accounting.common.sourcing.reactors.Reactor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AccountCreatedLogger implements Reactor<AccountCreated> {

    private final Logger logger = LoggerFactory.getLogger(AccountCreatedLogger.class);

    @Override
    public void react(AccountCreated event) {
        logger.info(String.format("Create account %s with balance %d", event.getNumber(), event.getBalance()));
    }
}
