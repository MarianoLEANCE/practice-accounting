package net.artificersoftware.accounting.account.model;

import net.artificersoftware.accounting.common.sourcing.aggregate.Aggregate;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

@Entity
public class Account {

    private AccountId id;
    private Integer balance;

    // constructors

    /**
     * For persistence only.
     */
    public Account() { }

    /**
     * Creates a new account.
     * The account will be created with a balance.
     */
    public Account(Aggregate aggregate, String number, Integer balance) {
        this.id = new AccountId(aggregate, number);
        this.balance = balance;
    }

    // methods

    public void deposit(Integer amount) {
        balance += amount;
    }

    public void withdraw(Integer amount) {
        balance -= amount;
    }

    // properties

    @EmbeddedId
    public AccountId getId() {
        return id;
    }

    public void setId(AccountId id) {
        this.id = id;
    }

    @Transient
    public String getNumber() {
        return id.getNumber();
    }

    @Transient
    public Aggregate getAggregate() {
        return id.getAggregate();
    }

    @NotNull
    @Column(name = "balance")
    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }
}
