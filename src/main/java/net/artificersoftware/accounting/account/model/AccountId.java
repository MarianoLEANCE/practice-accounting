package net.artificersoftware.accounting.account.model;

import net.artificersoftware.accounting.common.sourcing.aggregate.Aggregate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class AccountId implements Serializable {

    private Aggregate aggregate;
    private String number;

    // constructors

    public AccountId() {}

    public AccountId(Aggregate aggregate, String number) {
        this.aggregate = aggregate;
        this.number = number;
    }

    // properties

    @ManyToOne
    @JoinColumn(name = "aggregate_id")
    public Aggregate getAggregate() {
        return aggregate;
    }

    public void setAggregate(Aggregate aggregate) {
        this.aggregate = aggregate;
    }

    @Column(name = "number")
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
