package net.artificersoftware.accounting.account.service;

import net.artificersoftware.accounting.account.model.Account;
import net.artificersoftware.accounting.account.model.AccountId;
import net.artificersoftware.accounting.account.repository.AccountCreatedRepository;
import net.artificersoftware.accounting.account.repository.AccountRepository;
import net.artificersoftware.accounting.account.repository.MoneyDepositedRepository;
import net.artificersoftware.accounting.account.repository.MoneyWithdrawnRepository;
import net.artificersoftware.accounting.account.sourcing.calculators.AccountAddedCalculator;
import net.artificersoftware.accounting.account.sourcing.calculators.MoneyDepositedCalculator;
import net.artificersoftware.accounting.account.sourcing.calculators.MoneyWithdrawnCalculator;
import net.artificersoftware.accounting.account.sourcing.event.AccountCreated;
import net.artificersoftware.accounting.account.sourcing.event.MoneyDeposited;
import net.artificersoftware.accounting.account.sourcing.event.MoneyWithdrawn;
import net.artificersoftware.accounting.common.sourcing.aggregate.Aggregate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    private final AccountRepository accountRepository;

    private final AccountCreatedRepository accountCreatedRepository;
    private final AccountAddedCalculator accountAddedCalculator;

    private final MoneyDepositedRepository moneyDepositedRepository;
    private final MoneyDepositedCalculator moneyDepositedCalculator;

    private final MoneyWithdrawnRepository moneyWithdrawnRepository;
    private final MoneyWithdrawnCalculator moneyWithdrawnCalculator;

    // constructors

    public AccountService(AccountCreatedRepository accountCreatedRepository,
                          AccountAddedCalculator accountAddedCalculator,
                          AccountRepository accountRepository,
                          MoneyDepositedRepository moneyDepositedRepository,
                          MoneyDepositedCalculator moneyDepositedCalculator,
                          MoneyWithdrawnRepository moneyWithdrawnRepository,
                          MoneyWithdrawnCalculator moneyWithdrawnCalculator) {
        this.accountCreatedRepository = accountCreatedRepository;
        this.accountAddedCalculator = accountAddedCalculator;
        this.accountRepository = accountRepository;
        this.moneyDepositedRepository = moneyDepositedRepository;
        this.moneyDepositedCalculator = moneyDepositedCalculator;
        this.moneyWithdrawnRepository = moneyWithdrawnRepository;
        this.moneyWithdrawnCalculator = moneyWithdrawnCalculator;
    }

    // read operations

    /**
     * @param aggregate which aggregate to get the accounts from
     * @return the account list
     */
    public List<Account> list(Aggregate aggregate) {
        return accountRepository.findAllByIdAggregate(aggregate);
    }

    /**
     * @param aggregate which aggregate to find the account
     * @param number    the account number
     * @return an optional account
     */
    public Optional<Account> find(Aggregate aggregate, String number) {
        return accountRepository.findById(new AccountId(aggregate, number));
    }

    // write operations

    /**
     * Creates a new account with given number account and balance.
     * Registers an account created event.
     */
    public Account create(Aggregate aggregate, String number, Integer balance) {
        // create and save event
        AccountCreated event = new AccountCreated(number, balance);
        accountCreatedRepository.save(event);
        // handle event
        return accountAddedCalculator.calculateAndUpdateAndReact(event, aggregate);
    }

    public Optional<Account> deposit(Aggregate aggregate, Account account, Integer amount) {
        // create and save event
        MoneyDeposited event = new MoneyDeposited(account.getNumber(), amount);
        moneyDepositedRepository.save(event);
        // handle event
        return moneyDepositedCalculator.calculateAndUpdateAndReact(event, aggregate);
    }

    public Optional<Account> withdraw(Aggregate aggregate, Account account, Integer amount) {
        // create and save event
        MoneyWithdrawn event = new MoneyWithdrawn(account.getNumber(), amount);
        moneyWithdrawnRepository.save(event);
        // handle event
        return moneyWithdrawnCalculator.calculateAndUpdateAndReact(event, aggregate);
    }
}
