package net.artificersoftware.accounting.account.controller.request;

public class CreateAccountRequest {

    private String number;
    private Integer balance;

    public CreateAccountRequest() { }

    public CreateAccountRequest(String number, Integer balance) {
        this.number = number;
        this.balance = balance;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }
}
