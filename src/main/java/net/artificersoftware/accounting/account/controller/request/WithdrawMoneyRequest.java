package net.artificersoftware.accounting.account.controller.request;

public class WithdrawMoneyRequest {

    private Integer amount;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
