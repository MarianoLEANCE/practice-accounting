package net.artificersoftware.accounting.account.controller;

import net.artificersoftware.accounting.account.controller.request.CreateAccountRequest;
import net.artificersoftware.accounting.account.controller.request.DepositMoneyRequest;
import net.artificersoftware.accounting.account.controller.request.WithdrawMoneyRequest;
import net.artificersoftware.accounting.account.model.Account;
import net.artificersoftware.accounting.account.service.AccountService;
import net.artificersoftware.accounting.common.sourcing.aggregate.service.AggregateService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/{aggregateName}/accounts")
public class AccountController {

    private final AccountService accountService;
    private final AggregateService aggregateService;

    public AccountController(AccountService accountService,
                             AggregateService aggregateService) {
        this.accountService = accountService;
        this.aggregateService = aggregateService;
    }

    @Transactional
    @GetMapping
    public ResponseEntity<List<Account>> list(@PathVariable("aggregateName") String aggregateName) {
        return aggregateService.find(aggregateName)
                .map(accountService::list)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @Transactional
    @GetMapping("/{number}")
    public ResponseEntity<Account> read(@PathVariable("aggregateName") String aggregateName,
                                        @PathVariable("number") String number) {
        return aggregateService.find(aggregateName)
                .flatMap(aggregate -> accountService.find(aggregate, number))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @Transactional
    @PostMapping
    public ResponseEntity<Account> create(@PathVariable("aggregateName") String aggregateName,
                                          @RequestBody CreateAccountRequest createAccountRequest) {
        return aggregateService.find(aggregateName)
                .map(aggregate -> accountService.find(aggregate, createAccountRequest.getNumber())
                        .map(account -> new ResponseEntity<Account>(HttpStatus.CONFLICT))
                        .orElseGet(() -> ResponseEntity.ok(accountService.create(aggregate,
                                createAccountRequest.getNumber(),
                                createAccountRequest.getBalance()))))
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @Transactional
    @PostMapping("/{number}/deposit")
    public ResponseEntity<Account> deposit(@PathVariable("aggregateName") String aggregateName,
                                           @PathVariable("number") String number,
                                           @RequestBody DepositMoneyRequest request) {

        return aggregateService.find(aggregateName)
                .flatMap(aggregate -> accountService.find(aggregate, number))
                .flatMap(account -> accountService.deposit(account.getAggregate(), account, request.getAmount()))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @Transactional
    @PostMapping("/{number}/withdraw")
    public ResponseEntity<Account> withdraw(@PathVariable("aggregateName") String aggregateName,
                                            @PathVariable("number") String number,
                                            @RequestBody WithdrawMoneyRequest request) {
        return aggregateService.find(aggregateName)
                .flatMap(aggregate -> accountService.find(aggregate, number))
                .flatMap(account -> accountService.withdraw(account.getAggregate(), account, request.getAmount()))
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }
}
