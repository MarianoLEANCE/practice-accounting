package net.artificersoftware.accounting.common.sourcing.aggregate.controller;

import net.artificersoftware.accounting.account.controller.AccountController;
import net.artificersoftware.accounting.account.model.Account;
import net.artificersoftware.accounting.account.repository.AccountCreatedRepository;
import net.artificersoftware.accounting.account.repository.MoneyDepositedRepository;
import net.artificersoftware.accounting.account.repository.MoneyWithdrawnRepository;
import net.artificersoftware.accounting.account.sourcing.event.AccountCreated;
import net.artificersoftware.accounting.account.sourcing.event.MoneyDeposited;
import net.artificersoftware.accounting.account.sourcing.event.MoneyWithdrawn;
import net.artificersoftware.accounting.common.sourcing.aggregate.controller.dto.AggregateDTO;
import net.artificersoftware.accounting.common.sourcing.aggregate.controller.request.SynchronizeRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AggregateControllerIntegrationTest {

    @Autowired
    AggregateController controller;

    @Autowired
    AccountCreatedRepository accountCreatedRepository;
    @Autowired
    MoneyDepositedRepository moneyDepositedRepository;
    @Autowired
    MoneyWithdrawnRepository moneyWithdrawnRepository;

    @Autowired
    AccountController accountController;

    @Test
    @Transactional
    public void list() {
        controller.create("1");
        controller.create("2");
        controller.create("3");
        ResponseEntity<List<AggregateDTO>> list = controller.list();
        assertNotNull(list.getBody());
        assertEquals(3, list.getBody().size());
    }

    @Test
    @Transactional
    public void get() {
        controller.create("live");
        assertEquals(HttpStatus.OK, controller.get("live").getStatusCode());
        assertEquals(HttpStatus.NOT_FOUND, controller.get("dead").getStatusCode());
    }

    @Test
    @Transactional
    public void create() {
        ResponseEntity<AggregateDTO> aggregate = controller.create("live");
        assertNotNull(aggregate);
    }

    @Test
    @Transactional
    public void synchronize_AllEvents() {
        // creates 3 events resulting in an account with balance of 1500
        String accountNumber = "123456789";
        LocalDateTime event1Date =  LocalDate.of(2000, 1, 1).atStartOfDay();
        LocalDateTime event2Date =  LocalDate.of(2000, 1, 3).atStartOfDay();
        LocalDateTime event3Date =  LocalDate.of(2000, 1, 5).atStartOfDay();
        LocalDateTime synchronizationDate =  LocalDate.of(2000, 1, 10).atStartOfDay();

        AccountCreated accountCreated = new AccountCreated(accountNumber, 1000);
        accountCreated.setCreationDate(event1Date);
        accountCreatedRepository.save(accountCreated);
        // 1000
        MoneyDeposited moneyDeposited = new MoneyDeposited(accountNumber, 1000);
        moneyDeposited.setCreationDate(event2Date);
        moneyDepositedRepository.save(moneyDeposited);
        // 2000
        MoneyWithdrawn moneyWithdrawn = new MoneyWithdrawn(accountNumber, 500);
        moneyWithdrawn.setCreationDate(event3Date);
        moneyWithdrawnRepository.save(moneyWithdrawn);
        // 1500

        // create and synchronize an aggregate
        controller.create("live");
        SynchronizeRequest synchronizeRequest = new SynchronizeRequest();
        synchronizeRequest.setSynchronizationDate(synchronizationDate);
        ResponseEntity<AggregateDTO> aggregate = controller.synchronize("live", synchronizeRequest);

        assertNotNull(aggregate.getBody());
        assertEquals(event3Date, aggregate.getBody().getSynchronizationDate());
        ResponseEntity<Account> account = accountController.read("live", accountNumber);
        assertNotNull(account.getBody());
        assertEquals(1500, account.getBody().getBalance().intValue());
    }

    @Test
    @Transactional
    public void synchronize_snapshot() {
        // creates 3 events resulting in an account with balance of 1500
        String accountNumber = "123456789";
        LocalDateTime event1Date =  LocalDate.of(2000, 1, 1).atStartOfDay();
        LocalDateTime event2Date =  LocalDate.of(2000, 1, 3).atStartOfDay();
        LocalDateTime event3Date =  LocalDate.of(2000, 1, 5).atStartOfDay();
        LocalDateTime synchronizationDate =  LocalDate.of(2000, 1, 4).atStartOfDay();

        AccountCreated accountCreated = new AccountCreated(accountNumber, 1000);
        accountCreated.setCreationDate(event1Date);
        accountCreatedRepository.save(accountCreated);
        // 1000
        MoneyDeposited moneyDeposited = new MoneyDeposited(accountNumber, 1000);
        moneyDeposited.setCreationDate(event2Date);
        moneyDepositedRepository.save(moneyDeposited);
        // 2000
        MoneyWithdrawn moneyWithdrawn = new MoneyWithdrawn(accountNumber, 500);
        moneyWithdrawn.setCreationDate(event3Date);
        moneyWithdrawnRepository.save(moneyWithdrawn);
        // 1500

        // create and synchronize an aggregate between second and third event
        controller.create("snapshot");
        SynchronizeRequest synchronizeRequest = new SynchronizeRequest();
        synchronizeRequest.setSynchronizationDate(synchronizationDate);
        ResponseEntity<AggregateDTO> aggregate = controller.synchronize("snapshot", synchronizeRequest);

        assertNotNull(aggregate.getBody());
        assertEquals(event2Date, aggregate.getBody().getSynchronizationDate());
        ResponseEntity<Account> account = accountController.read("snapshot", accountNumber);
        assertNotNull(account.getBody());
        assertEquals(2000, account.getBody().getBalance().intValue());
    }
}
