package net.artificersoftware.accounting.account.controller;

import net.artificersoftware.accounting.account.controller.request.CreateAccountRequest;
import net.artificersoftware.accounting.account.model.Account;
import net.artificersoftware.accounting.common.sourcing.aggregate.service.AggregateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AccountControllerIntegrationTest {

    @Autowired
    AggregateService aggregateService;
    @Autowired
    AccountController accountController;

    @Test
    @Transactional
    public void list() {
        aggregateService.create("live");
        accountController.create("live", new CreateAccountRequest("1", 100));
        accountController.create("live", new CreateAccountRequest("2", 100));
        ResponseEntity<List<Account>> response = accountController.list("live");
        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().size());
    }

    @Test
    @Transactional
    public void read() {
        aggregateService.create("live");
        accountController.create("live", new CreateAccountRequest("1", 100));
        ResponseEntity<Account> response = accountController.read("live", "1");
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(100, response.getBody().getBalance().intValue());
    }

    @Test
    @Transactional
    public void read_notFound() {
        aggregateService.create("live");
        accountController.create("live", new CreateAccountRequest("1", 100));
        ResponseEntity<Account> response = accountController.read("live", "2");
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    @Transactional
    public void create() {
        aggregateService.create("live");
        ResponseEntity<Account> response = accountController.create("live", new CreateAccountRequest("1", 100));
        assertNotNull(response.getBody());
        assertEquals("1", response.getBody().getNumber());
        assertEquals(100, response.getBody().getBalance().intValue());
    }

    @Test
    @Transactional
    public void create_conflict() {
        aggregateService.create("live");
        accountController.create("live", new CreateAccountRequest("1", 100));
        ResponseEntity<Account> response = accountController.create("live", new CreateAccountRequest("1", 100));
        assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
    }

    @Test
    @Transactional
    public void deposit() {
    }

    @Test
    @Transactional
    public void withdraw() {
    }
}
