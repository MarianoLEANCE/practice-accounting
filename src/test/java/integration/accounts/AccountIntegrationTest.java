//package integration.accounts;
//
//import net.artificersoftware.accounting.Application;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import javax.transaction.Transactional;
//
//import static org.hamcrest.Matchers.hasSize;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = Application.class)
//@AutoConfigureMockMvc
//public class AccountIntegrationTest {
//
//    @Autowired
//    MockMvc mvc;
//
//    @Test
//    @Transactional
//    public void list() throws Exception {
//        mvc.perform(MockMvcRequestBuilders.get("/account/")
//                .accept(MediaType.APPLICATION_JSON))
//                .andDo(print())
//                .andExpect(jsonPath("$", hasSize(0)));
//    }

//    @Test
//    @Transactional
//    public void get() throws Exception {
//        mvc.perform(MockMvcRequestBuilders.get("/assets/2")
//                .accept(MediaType.IMAGE_PNG))
//                .andExpect(content().bytes(new byte[]{0xF}));
//    }
//
//    @Test
//    @Transactional
//    public void put() throws Exception {
//        MockMultipartFile firstFile = new MockMultipartFile("file",
//                "filename.txt",
//                "text/plain",
//                "some xml".getBytes());
//        MockMultipartHttpServletRequestBuilder builder = MockMvcRequestBuilders.multipart("/assets/")
//                .file(firstFile);
//
//        builder.with(request -> {
//            request.setMethod("PUT");
//            return request;
//        });
//
//        mvc.perform(MockMvcRequestBuilders.multipart("/assets/")
//                .file(firstFile))
//                .andExpect(status().isCreated());
//    }
//
//    @Test
//    @Transactional
//    public void post() throws Exception {
//
//        mvc.perform(MockMvcRequestBuilders.post("/account/")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content("{" +
//                        "\"amount\" : 0," +
//                        "\"currency\" : \"XPF\"" +
//                        "}"))
//                .andExpect(status().isCreated());
//        mvc.perform(MockMvcRequestBuilders.get("/account/")
//                .accept(MediaType.APPLICATION_JSON))
//                .andDo(print())
//                .andExpect(jsonPath("$", hasSize(1)));
//    }

//    @Test
//    @Transactional
//    public void delete() throws Exception {
//        mvc.perform(MockMvcRequestBuilders.delete("/assets/2"))
//                .andExpect(status().isOk());
//    }
//}
